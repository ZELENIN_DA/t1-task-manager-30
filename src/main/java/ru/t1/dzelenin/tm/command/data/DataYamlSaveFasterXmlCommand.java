package ru.t1.dzelenin.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.dto.Domain;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public final class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-yaml";
    @NotNull
    public static final String DESCRIPTION = "Save data in yaml file.";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE YAML]");

        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_YAML);
        @NotNull final Path path = file.toPath();

        Files.deleteIfExists(path);
        Files.createFile(path);

        @NotNull final ObjectMapper objectMapper = new YAMLMapper();
        @NotNull final String yaml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);

        fileOutputStream.write(yaml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}

