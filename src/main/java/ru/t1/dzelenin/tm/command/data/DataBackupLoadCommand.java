package ru.t1.dzelenin.tm.command.data;

import com.sun.istack.internal.Nullable;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.dto.Domain;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "backup-load";
    @NotNull
    public static final String DESCRIPTION = "Load backup from file.";

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull
        final byte[] base64Byte = Files.readAllBytes(Paths.get(FILE_BACKUP));
        @Nullable
        final String base64Data = new String(base64Byte);
        @Nullable
        final byte[] bytes = new BASE64Decoder().decodeBuffer(base64Data);
        @NotNull
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        @NotNull
        final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull
        final Domain domain = (Domain) objectInputStream.readObject();

        setDomain(domain);
        objectInputStream.close();
        byteArrayInputStream.close();
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}

