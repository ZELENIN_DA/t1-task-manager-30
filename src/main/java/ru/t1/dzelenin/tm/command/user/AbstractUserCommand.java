package ru.t1.dzelenin.tm.command.user;

import ru.t1.dzelenin.tm.api.service.IAuthService;
import ru.t1.dzelenin.tm.api.service.IUserService;
import ru.t1.dzelenin.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    public IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

}