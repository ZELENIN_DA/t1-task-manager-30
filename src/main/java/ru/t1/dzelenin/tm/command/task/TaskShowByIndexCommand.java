package ru.t1.dzelenin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.model.Task;
import ru.t1.dzelenin.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskShowCommand {

    private final String NAME = "task-show-by-index";

    private final String DESCRIPTION = "Display task by index.";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Task task = getTaskService().findOneByIndex(getUserId(), index);
        showTask(task);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
