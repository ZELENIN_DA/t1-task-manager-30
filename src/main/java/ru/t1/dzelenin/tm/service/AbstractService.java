package ru.t1.dzelenin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.api.repository.IRepository;
import ru.t1.dzelenin.tm.api.service.IService;
import ru.t1.dzelenin.tm.exception.entity.EntityNotFoundException;
import ru.t1.dzelenin.tm.exception.entity.ValueNullException;
import ru.t1.dzelenin.tm.exception.field.IdEmptyException;
import ru.t1.dzelenin.tm.exception.field.IndexIncorrectException;
import ru.t1.dzelenin.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;


public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final R repository;

    public AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    public M add(@NotNull final M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.add(model);
        return model;
    }

    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        if (comparator == null) findAll();
        return repository.findAll(comparator);
    }

    @NotNull
    @Override
    public M removeOne(@NotNull final M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.removeOne(model);
        return model;
    }

    @NotNull
    @Override
    public M findOneById(@NotNull final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final M model = repository.findOneById(id);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @NotNull
    @Override
    public M findOneByIndex(@NotNull final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @NotNull final M model = repository.findOneByIndex(index);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @NotNull
    @Override
    public M removeOneById(@NotNull final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeOneById(id);
    }

    @NotNull
    @Override
    public M removeOneByIndex(@NotNull final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        return repository.removeOneByIndex(index);
    }

    @NotNull
    @Override
    public void removeAll(@NotNull Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.removeAll(collection);
    }

    @NotNull
    @Override
    public Integer getSize() {
        return repository.getSize();
    }

    @NotNull
    @Override
    public boolean existsById(@NotNull String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @NotNull
    @Override
    public void removeAll() {
        repository.removeAll();
    }

    @NotNull
    public Collection<M> add(@Nullable final Collection<M> models) {
        if (models == null) throw new ValueNullException();
        return repository.add(models);
    }

    @NotNull
    public Collection<M> set(@Nullable final Collection<M> models) {
        if (models == null) throw new ValueNullException();
        return repository.set(models);
    }

}

