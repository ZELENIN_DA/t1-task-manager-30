package ru.t1.dzelenin.tm.exception.entity;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.exception.AbstractException;

@NoArgsConstructor
public abstract class AbstractEntityException extends AbstractException {

    public AbstractEntityException(@NotNull final String message) {
        super(message);
    }

    public AbstractEntityException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbstractEntityException(@NotNull final String message,
                                   @NotNull final Throwable cause,
                                   @NotNull final boolean enableSuppression,
                                   @NotNull final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }


}
