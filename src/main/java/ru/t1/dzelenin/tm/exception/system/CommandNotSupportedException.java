package ru.t1.dzelenin.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! This command is not supported...");
    }

    public CommandNotSupportedException(@NotNull final String command) {
        super("Error! Command ''" + command + "'' is not supported...");
    }

}
