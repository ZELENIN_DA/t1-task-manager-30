package ru.t1.dzelenin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    @NotNull
    Collection<AbstractCommand> getCommands();

    void add(@Nullable AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByName(String name);

    @Nullable
    AbstractCommand getCommandByArgument(String argument);

    @Nullable
    Iterable<AbstractCommand> getCommandsWithArgument();

}
