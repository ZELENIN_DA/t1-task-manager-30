package ru.t1.dzelenin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.api.repository.IUserOwnedRepository;
import ru.t1.dzelenin.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @NotNull
    @Override
    public void removeAll(@NotNull final String userId) {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @NotNull
    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) {
        return models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @NotNull final Comparator<M> comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) return null;
        return models.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return models.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M removeOneById(@NotNull final String userId, @NotNull final String id) {
        final M model = findOneById(userId, id);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @NotNull
    @Override
    public M add(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        return add(model);
    }

    @Nullable
    @Override
    public M removeOne(@NotNull final String userId, @NotNull final M model) {
        return removeOneById(userId, model.getId());
    }

    @NotNull
    @Override
    public int getSize(@NotNull final String userId) {
        return (int) models.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count();
    }

}


